import './Navbar.css';
import { NavLink } from 'react-router-dom'

export default function Navbar(){
    return(
        <div className={"bg-light"}>
            <div className={""}>
                <div className={"row"}>
                    <div className={"col-md-10"}>
                        <nav className="navbar navbar-expand-lg navbar-light">
                            <div className="collapse navbar-collapse" id="navbarNav">
                                <ul className="navbar-nav top-nested-links">
                                    <li className="nav-item active">
                                        <NavLink to="/" exact
                                                 >Home</NavLink>
                                    </li>
                                    <li className="nav-item active">
                                        <NavLink to="/about" exact activeStyle={{color:'red'}}
                                        >About Us</NavLink>
                                    </li>
                                    <li className="nav-item active">
                                        <NavLink to="/products" exact activeStyle={{color:'red'}}
                                        >Products</NavLink>
                                    </li>
                                    <li className="nav-item active">
                                        <NavLink to="/contact" exact activeStyle={{color:'red'}}
                                        >Contact Us</NavLink>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div className={"col-md-2 float-end"}>
                        <button type="button" className="btn btn-dark spacing-custom">Login</button>
                    </div>

                </div>
            </div>
        </div>
    );
}