import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {fetchProducts} from "../../redux/actions/ActionCreator";
import CategorizedProducts from "./CategorizedProducts";

export default function Jewellery(){
    const dispatch =useDispatch();
    useEffect(() => {
        dispatch(fetchProducts("jewelery"));
    }, []);
    return(
        <CategorizedProducts/>
    )
}
