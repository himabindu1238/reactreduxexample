import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {fetchProducts} from "../../redux/actions/ActionCreator";
import CategorizedProducts from "./CategorizedProducts";

export default function Electronics(){
    const dispatch =useDispatch();
    useEffect(() => {
        dispatch(fetchProducts("electronics"));
    }, []);
    return(
        <CategorizedProducts/>
    )
}
