import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {fetchProducts} from "../../redux/actions/ActionCreator";
import CategorizedProducts from "./CategorizedProducts";

export default function MensProducts(){
    const dispatch =useDispatch();
    useEffect(() => {
        dispatch(fetchProducts("men's clothing"));
    }, []);
    return(
        <CategorizedProducts/>
    )
}
