import {useEffect} from "react";
import {useDispatch} from "react-redux";
import {fetchProducts} from "../../redux/actions/ActionCreator";
import CategorizedProducts from "./CategorizedProducts";

export default function WomensProducts(){
    const dispatch =useDispatch();
    useEffect(() => {
            dispatch(fetchProducts("women's clothing"));
    }, []);
    return(
        <CategorizedProducts/>
    )
}
