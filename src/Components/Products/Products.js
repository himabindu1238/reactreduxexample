import React from "react";
import {NavLink, Outlet} from 'react-router-dom';

function Products(){
        return (
            <>
                <nav className={"nested-links"}>
                    <NavLink to="womens">Women Products</NavLink>
                    <NavLink to="mens">Mens Products</NavLink>
                    <NavLink to="jewelery">Jewelery Products</NavLink>
                    <NavLink to="electronics">Electronics</NavLink>
                </nav>

                <Outlet />
            </>
        );
}

export default Products;
