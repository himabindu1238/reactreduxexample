import "./Header.css";

export default function Header(){
    return(
        <div className={"header container-fluid"}>
            <div className={""}>
                <div className={"row"}>
                    <div className={"col-md-8"}>
                        <img className={"header-logo"} src={"./images/logo2.png"} />
                    </div>
                    <div className={"col-md-4"}>
                        <div className="main-search-input-wrap">
                            <div className="main-search-input fl-wrap">
                                <div className="main-search-input-item">
                                    <input type="text" value="" placeholder="Search Products..." />
                                </div>

                                <button className="main-search-button">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}