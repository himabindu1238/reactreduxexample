import './Footer.css';

export default function Footer(){
    return(
        <div className={"container-fluid bg-light"}>
            <div className={"row"}>

                <div className={"container"}>
                    <div className={"row"}>
                        <div className={"col-md-6 text-center py-3 footer-links"}>
                            <a href={""}>Home</a>|
                            <a href={""}>Best Sellers</a>|
                            <a href={""}>Mobiles</a>|
                            <a href={""}>Kitchen</a>|
                            <a href={""}>Electronics</a>
                        </div>
                        <div className={"col-md-6"}>
                            <p className={"text-center py-3"}>Copyright @2022. All rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}