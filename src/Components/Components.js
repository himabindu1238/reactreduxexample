import Header from "./HeaderComponent/Header";
import Navbar from "./NavBar/Navbar";
import Banner from "./Banner/Banner";
import Footer from "./Footer/Footer";
import Main from "./Main";

export default function Components(){
    return(
        <>
            <Header />
            <Navbar />
            <Banner />
            <Main />
            <Footer />
        </>
    );
}