import React, {Route, BrowserRouter as Router, Routes} from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Products from "./Products/Products";
import Contact from "./Contact";
import WomensProducts from "./Products/WomensProducts";
import MensProducts from "./Products/MensProducts";
import Jewellery from "./Products/Jewellery";
import Electronics from "./Products/Electronics";

export default function Main(){
    return(
        <Routes>
            <Route exact path="/" element={<Home />} ></Route>

            <Route exact path="/home" element={<Home />} />
            <Route exact path="/about" element={<About />} />
            <Route path="/products" element={<Products />} >
                <Route path="womens" element={<WomensProducts/>} />
                <Route path="mens" element={<MensProducts />} />
                <Route path="jewelery" element={<Jewellery />} />
                <Route path="electronics" element={<Electronics />} />
            </Route>

            <Route path="/contact" element={<Contact />} />
        </Routes>
    );
}