import ActionTypes from "../constants/action-types";
import fakeStoreAPI from "../../apis/fakeStoreAPI";

export const fetchProducts = (category) => {
    return async function(dispatch){
        const response = await fakeStoreAPI.get('/products');
        const products = response.data;
        const CategorizedProducts = products.filter(product => product.category === category);
        dispatch(setProducts(CategorizedProducts));
    }
}

export const setProducts = (products)=>{
    return{
        type:ActionTypes.SET_PRODUCTS,
        payload: products,
    }
}

